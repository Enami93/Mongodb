package com.mongodb;

import java.io.StringWriter;
import java.util.ArrayList;

import org.bson.Document;
import org.bson.codecs.DocumentCodec;
import org.bson.codecs.EncoderContext;
import org.bson.conversions.Bson;
import org.bson.json.JsonMode;
import org.bson.json.JsonWriter;
import org.bson.json.JsonWriterSettings;
import org.bson.types.ObjectId;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;

public class Hw_2_2 {

	public static void main(String[] args) {
	 MongoClient mongoClient = new MongoClient();
	    MongoDatabase db = mongoClient.getDatabase("students");
	    final MongoCollection<Document> collection = db.getCollection("grades");
	    
	    Bson sort =  Sorts.orderBy(Sorts.descending("student_id"), Sorts.ascending("score"));
	    
	    
	    ArrayList<Document>  all = collection.find().sort(sort).into(new ArrayList<Document>());
	  
	   ArrayList<Document>  lowestScores  = getLowestScores(all);
	 
	   for(Document cur: lowestScores) {
	    	printJson(cur);
	    	collection.deleteOne(cur);
	    }   
	    for(Document cur: all) {
	    	printJson(cur);

	    } 
	}

	  public static void printJson(Document document) {
		    JsonWriter jsonWriter = new JsonWriter(new StringWriter(), new JsonWriterSettings(JsonMode.SHELL, true));
		    new DocumentCodec().encode(jsonWriter, document, EncoderContext.builder().isEncodingCollectibleDocument(true).build());
		    System.out.println(jsonWriter.getWriter());
		    System.out.flush();
		  }
	  
	  public static ArrayList<Document> getLowestScores(ArrayList scores) {
		  ArrayList<Document> tmp = new ArrayList<Document>();
		  for (int i = 0 ; i < scores.size() ;  i++) {
				  if(i % 2 == 0 ) {
					  tmp.add((Document) scores.get(i));
				  }  	
			  }
		  return tmp;
		  }
	  }

