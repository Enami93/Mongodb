package com.mongodb;
import java.io.StringWriter;
import java.util.ArrayList;

import org.bson.Document;
import org.bson.codecs.DocumentCodec;
import org.bson.codecs.EncoderContext;
import org.bson.conversions.Bson;
import org.bson.json.JsonMode;
import org.bson.json.JsonWriter;
import org.bson.json.JsonWriterSettings;

import com.mongodb.*;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Filters.*;
import com.mongodb.client.model.Sorts;

import freemarker.template.Configuration;
import freemarker.template.Template;
import spark.Request;
import spark.Response;
import spark.Route;
import spark.Spark;

public class Hw2_1 {
	
	public static void main(String[] args) {
	

	    MongoClient mongoClient = new MongoClient();
	    MongoDatabase db = mongoClient.getDatabase("students");
	    final MongoCollection<Document> collection = db.getCollection("grades");
	    
	    Bson filter =  Filters.and(Filters.gte("score",65));
	    Bson sort =  Sorts.ascending("score");
	    
	    ArrayList<Document>  all = collection.find(filter).sort(sort).into(new ArrayList<Document>());
	    

	    for(Document cur: all) {
	    	printJson(cur);
	    }

}
	  public static void printJson(Document document) {
		    JsonWriter jsonWriter = new JsonWriter(new StringWriter(), new JsonWriterSettings(JsonMode.SHELL, true));
		    new DocumentCodec().encode(jsonWriter, document, EncoderContext.builder().isEncodingCollectibleDocument(true).build());
		    System.out.println(jsonWriter.getWriter());
		    System.out.flush();
		  }
}
